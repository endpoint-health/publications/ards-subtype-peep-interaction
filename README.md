Please find the statistical analysis plan in the file: SAP_HTE_v1.1_final.docx.  (Uploaded January 29, 2021)

An earlier version (uploaded December 7, 2020) can be seen in SAP_HTE_v1.0.docx.  The difference between version 1.0 and 1.1 is an update to the clustering training strategy and some minor typo corrections.  No changes were made to the Bayesian analysis plan.
